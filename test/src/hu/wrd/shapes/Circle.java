package hu.wrd.shapes;

public class Circle implements Shape {

    private int radius;
    Circle(int radius) {

        this.radius = radius;
    }

    @Override
    public int area() {
        return (int)(radius*radius*Math.PI);
    }


}
